#%%

#%%
from pw.networks import unet_model_builder,DummyNet_builder,rgrow_net
from pw.patchwork import deep_patch_work
from pw.data import PWData,pw_img_data
import pw.tools as pw_tools
from pw.units import rebrand,UNet_factory,tracing_layer

import types
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


import glob
import nibabel as nib
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib as mpl
dpi_default = mpl.rcParams['figure.dpi']
import time
import os


#tl = tracing_layer(2)
#img = torch.zeros([4,9,42,42])    
#y = tl(img)


dim = 2
n_modalities = 2
n_labels = 1
pfield = (32,32)

%config InlineBackend.figure_format = 'retina'





#%%
import imageio
import numpy as np
img_ = imageio.imread('~/projects/deep_patchwork/trace_tst/gt.png').astype(np.float32)
img_2 = imageio.imread('~/projects/deep_patchwork/trace_tst/gt3.png').astype(np.float32)
img = img_2[...,2]/255 + 0.5*np.random.randn(img_.shape[0],img_.shape[1])
seeds = (img_[...,0]>img_[...,1])*(img_[...,0]>254)
label = 1 - img_[...,2]/255

element_size = (1,1)
dim = 2

#normalization = "-mean/std" 
normalization = "none" 
dataset_train = PWData(n_modalities = n_modalities,
                n_labels = n_labels,
                dim = dim)

img_d = np.concatenate((img[None,...],seeds[None,...]),axis=0)
dataset_train.add_dataset(
                 img_d,
                 label[None,...],
                 normalization = normalization,
                 element_size = element_size,
                 keep_pdf = False,
                 debug = False,
                 pdf_img = None,
                )

#%%

#%%

def activation(tensor,indx=None,not_used=True):
    return tensor
    
dcount = 0
def regloss(prediction, 
                labels,
                patch_imgs,
                writer,
                n_scales,
                activation,
                start_scale=0,
                ):
        
        loss = 0
        correct = 0
        total = 0
        bce = nn.BCELoss(reduction="mean")
        mse = nn.MSELoss(reduction="mean")
        global dcount
        dcount += 1
        
        freq = 50
        #freq = 1
        

        for s in range(start_scale,n_scales):
            
                inputs = patch_imgs.patchlist[s].tensor[:,:n_modalities,...]
                gt = labels.patchlist[s].tensor[:,:-1,...]
                #pi = 0
                if s == 0:
                    gt_0 = inputs[:,1,None,...].detach().cpu()
                    valid_idx = gt_0.amax(dim=(1,2,3))
                    pi = torch.where((valid_idx>0.5)>0)
                    print(pi)
                    if len(pi[0])>0:
                        pi = pi[0][0]
                    else:
                        pi = 0
                    gt_0 = gt_0[pi,...]
                        
                valid = labels.patchlist[s].tensor[:,-1,None,...] > 0
                
                pred = prediction[str(s)][:,:2,...]
                
               # print("###")
                #print(pred[0].amin()," ",pred[0].amax())
                #print(pred[1].amin()," ",pred[1].amax())
                
                if s == n_scales-1:
                    l = bce(pred[:,0,None,...][valid],gt[:,0,None,...][valid])
                else:
                    l = bce(pred[:,0,None,...][valid],gt[:,0,None,...][valid])
                    #l = mse(pred[:,0,None,...][valid],gt[:,0,None,...][valid])
                loss += l
                show_patches =[0]
                
                if dcount%freq==0: 
                    if s == 0:
                        #print(pi)
                        #print(pred[pi,[0,0],...].detach().cpu().shape)
                        #print(gt_0.shape)
                        dd = torch.cat((0.75*pred[pi,0,None,...].detach().cpu(),gt_0),dim=0)
                        #print(pred[pi,[0,0],...].detach().cpu().amin()," ",pred[pi,[0,0],...].detach().cpu().amax())
                        #print(gt_0.amin()," ",gt_0.detach().cpu().amax())
                    else:
                        dd_ = torch.cat((0.75*pred[pi,0,None,...].detach().cpu(),gt_0),dim=0)
                        dd = torch.cat((dd,dd_),dim=2)
                #if dcount%100==0:
                if dcount%freq==0 and s == 0:
                #if False   
                    
                #if s == 0:
                #if True:
                        for pi in show_patches:
                            in_ = inputs[pi,[0,0,0],...].clone()
                            in_ -= in_.amin()
                            in_ /= in_.amax()+0.0001
                            plt.imshow(
                                torch.permute(torch.cat( (
                                in_,
                                gt[pi,[0,0,0],...],
                                pred[pi,[0,0,0],...].detach(),
                                pred[pi,[1,1,1],...].detach(),
                                pred[pi,[0,1,1],...].detach(),
                                ),
                                dim=2),(1,2,0)).cpu(),vmin=0,vmax=1)
                            plt.axis('off')
                            plt.show()
                                
        loss /= n_scales
        
        if dcount%freq==0:
            dummy = torch.zeros(dd.shape)
            
            plt.imshow(torch.permute(torch.cat((dd,dummy[0,None,]),dim=0),(1,2,0)))
            plt.axis('off')
            plt.show()
        # I forgot for what the second value was.
        return loss,1,None    


#%%

patchlevels = 10
patchlevels = 2

arc = rgrow_net
Net_settings = {
            "fin":None,
            "fout":None,
            "depth":5,
            "noskip":[],
            "norm":'BatchNorm',
            "conv_block":"conv_block",
            "dropouts":0.0,
            "skip_type":"3x3",
            "feat_fun":"lambda d:16*(d+1)",
            "skip_feat_fun":"lambda d:16*(d+1) //2",
            "up_feat_fun":"lambda d:16*(d+1) // 2",
            "pfield":None,
            "upsampling":"conv",
            "probabilistic_enc":False,
            "trace_net":
                {
                "modalities":2,
                "has_initial_labels":False,
                "do_segmentation":True,
                "positive_constraint":True,
                "tracing_threshold":0.1,
                "segmentation_threshold":0.5
                }
}


copy = {}
for a in range(0,patchlevels-2):
    copy[a+1]=0
    
pw_layout={
    "verbose":1,
    "dim":dim,
    "fin":n_modalities,
    "labels":2,    
    "normalize_inputs":None,
    "prop_norm":None,
    "pfield":pfield,
    "scales":
    {
        "params":Net_settings,
        "arc":arc,
        "patchlevels":patchlevels,
        "out":2,
    },
    "copy":copy
}
    
mynet = deep_patch_work(pw_layout=pw_layout,verbose=True)

print("estimating patch parameters")
cover = 1.0

target_element_size_scale = 1.0
isotropic = True
patch_params = dataset_train.estimate_parameters(depth=patchlevels,
                                                 cover=cover,
                                                 pfield=pfield,
                                                 isotropic=isotropic,
                                                 target_element_size_scale=target_element_size_scale)
patch_params["scale_facts"][:] = 1
#%%
new_net = True

torch.autograd.set_detect_anomaly(True)
torch.cuda.empty_cache()


Testrun = False
sampling = "pdf_coarse2fine"

train_device = "cuda"
aug_device = "cuda"

batch_size = 10
resample = 1
patchbuffersize = 1000



Testrun = True

if Testrun:
    resample = 1
    batch_size = 10
    patchbuffersize = 10
    aug_device = "cpu"
    train_device = "cpu"

    #aug_device = "cuda"
    #train_device = "cuda"

augmentation = {    
            "aug_rot":360,
            "aug_rot_mode":"xy",
            "aug_rot_keep_mat":True,
            }


intensity_augmentation = None
prior = None

weights = {'bg':1,'classes':[1],"sample_from_class_indx":[-1]}
pw_loss = {"loss":regloss,"params":{"activation":activation,}}


for a in range(1 if Testrun else 10000000):
        mynet.train_epoch(
                  augmentation = augmentation,
                  intensity_augmentation=intensity_augmentation,
                  sampling = "pdf_coarse2fine",
                  aug_device = aug_device,
                  patchbuffersize = patchbuffersize,
                  weights = weights,
                  snap = "valid",
                  scale_facts = patch_params["scale_facts"],
                  sampler_debug_scales = 0,
                  target_element_size = patch_params["target_element_size"],
                  batch_size = batch_size,
                  resample = resample,
                  train_device = train_device,
                  optimizer = {"optim":optim.AdamW,"params":{"lr":0.001, "eps":1e-07}},
                  #scheduler = {"scheduler":ExponentialLR,"params":{"gamma":0.995}},  
                  dataset_train = dataset_train,
                  #writer = writer,
                  clear = new_net,
                  pw_loss = pw_loss,
                  do_resampling=True)

        # we set this to false so that we can restart anytime without resetting patchwork training params    
        new_net = False
    
    
